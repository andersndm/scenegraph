using System.Numerics;

public enum GraphicType
{
	None,
	Cube,
	Sphere
}

public class Graphic
{
	private GraphicType graphicType;
	private Vector3 color;

	public Graphic(GraphicType graphicType, Vector3 color)
	{
		this.graphicType = graphicType;
		this.color = color;
	}

	public GraphicType getGraphicType()
	{
		return graphicType;
	}

	public Vector3 getColor()
	{
		return color;
	}
}

public class GraphicNone : Graphic
{
	public GraphicNone() : base(GraphicType.None, Vector3.Zero) {}
}

public class GraphicCube : Graphic
{
	public GraphicCube(Vector3 color) : base(GraphicType.Cube, color) {}
}

public class GraphicSphere : Graphic
{
	public GraphicSphere(Vector3 color) : base(GraphicType.Sphere, color) {}
}