using System.Numerics;
using System.Collections.Immutable;

public class Node
{
	private Vector3 basePosition;
	private Vector3 position;

	private IMovement movement;
	private Graphic graphic;
	private List<Node> children;

	public Node(Vector3 offset, IMovement movement, Graphic graphic)
	{
		basePosition = offset;
		this.position = basePosition;
		this.movement = movement;
		this.graphic = graphic;
		children = new List<Node>();
	}

	public void addChild(Node child)
	{
		children.Add(child);
	}

	public void removeChild(int index)
	{
		children.RemoveAt(index);
	}

	public void update(float dt, Vector3 parentPosition)
	{
		movement.update(dt);
		position = parentPosition + basePosition + movement.getPosition();
	}

	public void updateChildren(float dt)
	{
		foreach (Node child in children)
		{
			child.update(dt, position);
			child.updateChildren(dt);
		}
	}

	public ImmutableList<Node> getChildren()
	{
		return children.ToImmutableList<Node>();
	}

	public Graphic getGraphic()
	{
		return graphic;
	}

	public Vector3 getPosition()
	{
		return position;
	}
}