﻿"use strict";

import * as THREE from 'three';

const scene = new THREE.Scene();
scene.background = new THREE.Color(0xeeeeee);
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight,
										   0.1, 1000);
camera.position.z = 7;

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

var connection = new signalR.HubConnectionBuilder().withUrl("/scenegraphHub").build();

connection.on("ReceiveNode", function (node) {
	if (node.type == 1)
	{
		const geometry = new THREE.BoxGeometry(1, 1, 1);
		const material = new THREE.MeshBasicMaterial({ color: new THREE.Color(node.r, node.g, node.b) });
		const mesh = new THREE.Mesh(geometry, material);
		mesh.position.x = node.x;
		mesh.position.y = node.y;
		mesh.position.z = node.z;
		scene.add(mesh);
	}
	if (node.type == 2)
	{
		const geometry = new THREE.SphereGeometry(0.5);
		const material = new THREE.MeshBasicMaterial({ color: new THREE.Color(node.r, node.g, node.b) });
		const mesh = new THREE.Mesh(geometry, material);
		mesh.position.x = node.x;
		mesh.position.y = node.y;
		mesh.position.z = node.z;
		scene.add(mesh);
	}
});

connection.on("AllNodesSent", function () {
	renderer.render(scene, camera);
	scene.clear();
});

connection.start()

function animate()
{
	setTimeout( function() {

        requestAnimationFrame( animate );

    }, 1000 / 30 );
	connection.invoke("RequestSceneGraph").catch(function (err) {
		return console.error(err.toString());
	});
}

animate();