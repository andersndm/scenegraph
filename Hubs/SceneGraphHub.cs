using Microsoft.AspNetCore.SignalR;
using System.Numerics;
using System.Diagnostics;

public class SceneGraphHub : Hub
{
	SceneGraphRoot sceneGraph;

	public SceneGraphHub(SceneGraphRoot sceneGraph)
	{
		this.sceneGraph = sceneGraph;
	}

	public async Task RequestSceneGraph()
	{
		sceneGraph.update();

		List<Command> commands = new List<Command>();
		sceneGraph.fillCommandList(Clients.Caller, commands);
		commands.Add(Command.CommandNodesSent(Clients.Caller));

		foreach (Command command in commands)
		{
			await command.sendCommand();
		}
	}
}