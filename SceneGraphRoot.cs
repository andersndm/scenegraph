using System.Numerics;
using System.Diagnostics;
using Microsoft.AspNetCore.SignalR;

public class SceneGraphRoot
{
	private Node rootNode;
	private Stopwatch stopwatch;

	public SceneGraphRoot()
	{
		rootNode = new Node(new Vector3(0f, -2f, 0f), new MovementNone(), new GraphicCube(new Vector3(0f, 0f, 1f)));
		Node rBox = new Node(new Vector3(0f, 2f, 0f),
							 new MovementLinear(new Vector3(-1f, 0f, 0f), new Vector3(1f, 0f, 0f), 1f),
							 new GraphicCube(new Vector3(1f, 0f, 0f)));
		Node gSphere = new Node(new Vector3(0f, 2f, 0f),
								new MovementCircular(2f, 0f, 1f),
								new GraphicSphere(new Vector3(0f, 1f, 0f)));
		Node mBox = new Node(new Vector3(0f, 0f, 0f),
							 new MovementLinear(new Vector3(-1f, 0f, 0f), new Vector3(1f, 0f, 0f), 1f),
							 new GraphicCube(new Vector3(1f, 0f, 1f)));
		gSphere.addChild(mBox);
		rBox.addChild(gSphere);
		rootNode.addChild(rBox);
		Node gBox = new Node(new Vector3(-2f, 2f, 0f),
							 new MovementLinear(new Vector3(0f, -2f, 0f), new Vector3(0f, 2f, 0f), 0.2f),
							 new GraphicCube(new Vector3(0f, 1f, 0f)));
		rootNode.addChild(gBox);
		Node ySphere = new Node(new Vector3(4f, 2f, 0f),
								new MovementCircular(1f, MathF.PI, 2f),
								new GraphicSphere(new Vector3(1f, 1f, 0f)));
		rootNode.addChild(ySphere);
		stopwatch = new Stopwatch();
		stopwatch.Start();
	}

	public void update()
	{
		stopwatch.Stop();
		long elapsedMS = stopwatch.ElapsedMilliseconds;
		if (elapsedMS > (1000 / 30))
		{
			float dt = (float)elapsedMS / 1000f;
			rootNode.update(dt, Vector3.Zero);
			rootNode.updateChildren(dt);
			stopwatch.Reset();
		}
		stopwatch.Start();
	}

	private void fillCommandList(ISingleClientProxy client, Node node, List<Command> commands)
	{
		commands.Add(new NodeCommand(client, node.getGraphic(), node.getPosition()));
		foreach (Node child in node.getChildren())
		{
			fillCommandList(client, child, commands);
		}
	}

	public void fillCommandList(ISingleClientProxy client, List<Command> commands)
	{
		fillCommandList(client, rootNode, commands);
	}
}