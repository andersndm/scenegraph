using Microsoft.AspNetCore.SignalR;
using System.Numerics;
using System.Text.Json;
public class NodeCommand : Command
{
	private Graphic graphic;
	private Vector3 position;

	public NodeCommand(ISingleClientProxy client, Graphic graphic, Vector3 position) : base(client, "ReceiveNode")
	{
		this.graphic = graphic;
		this.position = position;
	}

	private class NodeData
	{
		public int type { get; set; }
		public float r { get; set; }
		public float g { get; set; }
		public float b { get; set; }
		public float x { get; set; }
		public float y { get; set; }
		public float z { get; set; }		
	}

	public async override Task sendCommand()
	{
		Vector3 color = graphic.getColor();

		NodeData data = new NodeData
		{
			type = (int)graphic.getGraphicType(),
			r = color.X,
			g = color.Y,
			b = color.Z,
			x = position.X,
			y = position.Y,
			z = position.Z
		};

		await client.SendAsync(function, data);
	}
}