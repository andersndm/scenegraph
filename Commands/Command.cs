using Microsoft.AspNetCore.SignalR;

public class Command
{
	protected ISingleClientProxy client;
	protected string function;

	public Command(ISingleClientProxy client, string function)
	{
		this.client = client;
		this.function = function;
	}

	public async virtual Task sendCommand()
	{
		await client.SendAsync(function);
	}

	public static Command CommandNodesSent(ISingleClientProxy client)
	{
		return new Command(client, "AllNodesSent");
	}

	public static Command CommandUpdated(ISingleClientProxy client)
	{
		return new Command(client, "SceneGraphUpdated");
	}
}