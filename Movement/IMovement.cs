using System.Numerics;

public interface IMovement
{
	void update(float dt);
	Vector3 getPosition();
}