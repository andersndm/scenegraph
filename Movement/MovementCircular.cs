using System.Numerics;

public class MovementCircular : IMovement
{
	private float radius;
	private float phaseOffset;
	private float speed;
	private float angle;

	public MovementCircular(float radius, float phaseOffset, float speed)
	{
		this.radius = radius;
		this.phaseOffset = phaseOffset;
		this.speed = speed;
		this.angle = 0.0f;
	}


	public void update(float dt)
	{
		angle += speed * dt;
		if (angle > 2f * MathF.PI)
		{
			angle -= 2f * MathF.PI;
		}
	}

	public Vector3 getPosition()
	{
		float x = radius * MathF.Cos(angle + phaseOffset);
		float y = radius * MathF.Sin(angle + phaseOffset);
		return new Vector3(x, y, 0.0f);
	}
}