using System.Numerics;

public class MovementNone : IMovement
{
	public MovementNone() {}

	public void update(float dt) {}
	public Vector3 getPosition()
	{
		return Vector3.Zero;
	}
}