using System.Numerics;

public class MovementLinear : IMovement
{
	private readonly Vector3 min;
	private readonly Vector3 max;
	private float speed;
	private float i;

	public MovementLinear(Vector3 min, Vector3 max, float speed)
	{
		this.min = min;
		this.max = max;
		this.speed = speed;
		i = 0f;
	}

	public void update(float dt)
	{
		i += dt * speed * MathF.PI;
		if (i > 2f * MathF.PI)
		{
			i -= 2f * MathF.PI;
		}
	}

	public Vector3 getPosition()
	{
		return Vector3.Lerp(min, max, 0.5f * MathF.Cos(i) + 0.5f);
	}
}